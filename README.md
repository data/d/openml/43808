# OpenML dataset: German-Credit-Data

https://www.openml.org/d/43808

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The original dataset contains 1000 entries with 20 categorial/symbolic attributes prepared by Prof. Hofmann. In this dataset, each entry represents a person who takes a credit by a bank. Each person is classified as good or bad credit risks according to the set of attributes. The link to the original dataset can be found below.
Content
It is almost impossible to understand the original dataset due to its complicated system of categories and symbols. Thus, I wrote a small Python script to convert it into a readable CSV file. The column names were also given in German originally. So, they are replaced by English names while processing. The attributes and their details in English are given below:

Status - Categorical (Ordinal)
Duration    - Numerical
Credit History  - Categorical (Nominal)
Purpose - Categorical (Nominal)
Amount - Numerical
Savings - Categorical (Ordinal)
Employment Duration - Categorical (Ordinal)
Installment Rate - Categorical (Ordinal)
Personal Status Sex - Categorical (Nominal)
Other Debtors - Categorical (Nominal)
Present Residence - Categorical (Ordinal)
Property - Categorical (Nominal)
Age - Numerical
Other Installment Plans - Categorical (Nominal)
Housing - Categorical (Nominal)
Number Credits - Categorical (Ordinal)
Job - Categorical (Nominal)
People Liable - Categorical (Ordinal)
Telephone - Categorical (Nominal)
Foreign Worker - Categorical (Nominal)
Credit Risk - Binary Target Variable

Acknowledgements
Source : UCI

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43808) of an [OpenML dataset](https://www.openml.org/d/43808). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43808/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43808/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43808/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

